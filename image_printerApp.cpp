/***************************************************************
 * Name:      image_printerApp.cpp
 * Purpose:   Code for Application Class
 * Author:    JJ Hsieh ()
 * Created:   2014-08-01
 * Copyright: JJ Hsieh ()
 * License:
 **************************************************************/

#include "wx_pch.h"
#include "image_printerApp.h"
#include <wx/print.h>
#include <wx/cmdline.h>
#include <wx/filename.h>
#include <wx/stdpaths.h>

static const wxCmdLineEntryDesc g_cmdLineDesc[] = {
    { wxCMD_LINE_SWITCH, "h", "help", _("displays help on the command line parameters") },
    { wxCMD_LINE_SWITCH, "v", "version", _("print version") },
    { wxCMD_LINE_OPTION, "d", "debug", _("specify a debug level"), wxCMD_LINE_VAL_NUMBER },
    { wxCMD_LINE_PARAM, NULL, NULL, _("specify input file"), wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
    { wxCMD_LINE_NONE }
};

//(*AppHeaders
#include "image_printerMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(image_printerApp);

void image_printerApp::init_language(void)
{
    this->language = wxLANGUAGE_DEFAULT;

    // fake functions, use proper implementation
    //if( userWantsAnotherLanguageThanDefault() )
    //    language = getUsersFavoriteLanguage();

    // load language if possible, fall back to english otherwise
    if(wxLocale::IsAvailable(this->language))
    {
        this->locale = new wxLocale(this->language);

#ifdef __WXGTK__
        // add locale search paths
        this->locale->AddCatalogLookupPathPrefix(wxT("."));
        this->locale->AddCatalogLookupPathPrefix(wxT("/usr"));
        this->locale->AddCatalogLookupPathPrefix(wxT("/usr/local"));
        wxStandardPaths* paths = (wxStandardPaths*) &wxStandardPaths::Get();
        wxString prefix = paths->GetInstallPrefix();
        this->locale->AddCatalogLookupPathPrefix(prefix);
#endif

        const wxLanguageInfo* LanguageInfo = wxLocale::GetLanguageInfo(this->language);
        this->locale->AddCatalog("wxstd");
        if (!this->locale->AddCatalog(APP_EXEC_NAME)) {
            wxLogError(_("Couldn't find/load the catalog for locale '%s'."),
                   LanguageInfo ? LanguageInfo->GetLocaleName() : _("unknown"));
        }

        if(!this->locale->IsOk()) {
            std::cerr << "selected language is wrong" << std::endl;
            delete this->locale;
            this->locale = new wxLocale(wxLANGUAGE_ENGLISH);
            this->language = wxLANGUAGE_ENGLISH;
        }
    } else {
        std::cout << "The selected language is not supported by your system."
                  << "Try installing support for this language." << std::endl;
        this->locale = new wxLocale(wxLANGUAGE_ENGLISH);
        this->language = wxLANGUAGE_ENGLISH;
    }

}

bool image_printerApp::OnInit()
{
    // set language
    this->init_language();

    //(*AppInitialize
    bool wxsOK = true;
    image_printerFrame* Frame = NULL;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
        Frame = new image_printerFrame(0);
        Frame->Show();
        SetTopWindow(Frame);
    }
    //*)

    // Parse command line
    wxString cmdFilename;
    wxCmdLineParser cmdParser(g_cmdLineDesc, argc, argv);
    int res;
    {
        wxLogNull log;
        // Pass false to suppress auto Usage() message
        res = cmdParser.Parse(false);
    }

    // Check if the user asked for command-line help
    if (res == -1 || res > 0 || cmdParser.Found("h")) {
        cmdParser.Usage();
        return false;
    }

    // Check if the user asked for the version
    if (cmdParser.Found("v")) {
        wxString msg;
        wxString date (wxString::FromAscii(__DATE__));
        msg.Printf (_("%s, Version %s, %s\n\n%s"), APP_NAME, APP_VERSION, (const wxChar*) date, APP_LICENSE);
        wxPrintf (msg);
        return false;
    }

    // Check for debug level
    long debug_level = 0;
    if (cmdParser.Found("d", &debug_level)) {
            this->debug_level = debug_level;
    }

    // Check for a project filename
    if (cmdParser.GetParamCount() > 0)
    {
        cmdFilename = cmdParser.GetParam (0);
        // Under Windows when invoking via a document
        // in Explorer, we are passed the short form.
        // So normalize and make the long form.
        wxFileName file_name (cmdFilename);
        file_name.Normalize(wxPATH_NORM_LONG|wxPATH_NORM_DOTS|wxPATH_NORM_TILDE|wxPATH_NORM_ABSOLUTE);
        if (file_name.FileExists()) {
            Frame->OpenFile(file_name.GetFullPath());
        }
    }

    return wxsOK;
}
