/***************************************************************
 * Name:      image_printerApp.h
 * Purpose:   Defines Application Class
 * Author:    JJ Hsieh ()
 * Created:   2014-08-01
 * Copyright: JJ Hsieh ()
 * License:
 **************************************************************/

#ifndef IMAGE_PRINTERAPP_H
#define IMAGE_PRINTERAPP_H

#include <wx/app.h>

#define APP_NAME _("ImagePrinter")
#define APP_VERSION _("1.0.0")
#define APP_EXEC_NAME _("image-printer")
#define APP_AUTHOR _("JJ Hsieh")
#define APP_AUTHOR_MAIL _("jj.elite@gmail.com")
#define APP_WEB_SITE _("https://github.com/jjhsieh/image-printer")
#define APP_LICENSE _("\
Copyright (C) 2014 JJ Hsieh\n\
\n\
This program is free software; you can redistribute it and/or\n\
modify it under the terms of the GNU General Public License\n\
as published by the Free Software Foundation; either version 2\n\
of the License, or (at your option) any later version.\n\
\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n\
GNU General Public License for more details.\n\
\n\
You should have received a copy of the GNU General Public License\n\
along with this program; if not, write to the Free Software\n\
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.\n\
")

class image_printerApp : public wxApp
{
    private:
        long debug_level;
        wxLocale* locale;
        long language;
        void init_language(void);
    public:
        virtual bool OnInit();

};

wxDECLARE_APP(image_printerApp);

#endif // IMAGE_PRINTERAPP_H
