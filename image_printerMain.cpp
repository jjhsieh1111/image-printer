/***************************************************************
 * Name:      image_printerMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    JJ Hsieh ()
 * Created:   2014-08-01
 * Copyright: JJ Hsieh ()
 * License:
 **************************************************************/

#include "wx_pch.h"
#include "image_printerApp.h"
#include "image_printerMain.h"
#include <wx/msgdlg.h>
#include <wx/aboutdlg.h>

//(*InternalHeaders(image_printerFrame)
#include <wx/string.h>
#include <wx/intl.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/artprov.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(image_printerFrame)
const long image_printerFrame::ID_STATICBITMAP1 = wxNewId();
const long image_printerFrame::ID_SCROLLEDWINDOW1 = wxNewId();
const long image_printerFrame::idMenuOpenFile = wxNewId();
const long image_printerFrame::idMenuCloseFile = wxNewId();
const long image_printerFrame::idMenuPrint = wxNewId();
const long image_printerFrame::idMenuPrintSetup = wxNewId();
const long image_printerFrame::idMenuPrintPreview = wxNewId();
const long image_printerFrame::idMenuQuit = wxNewId();
const long image_printerFrame::idMenuAbout = wxNewId();
const long image_printerFrame::ID_STATUSBAR = wxNewId();
const long image_printerFrame::ID_TOOLBARITEMOPEN = wxNewId();
const long image_printerFrame::ID_TOOLBARITEMPRINT = wxNewId();
const long image_printerFrame::ID_TOOLBAR1 = wxNewId();
const long image_printerFrame::ID_POPUP_PRINT = wxNewId();
//*)

const long image_printerFrame::ID_BITMAPBUTTON1 = wxNewId();

BEGIN_EVENT_TABLE(image_printerFrame,wxFrame)
    //(*EventTable(image_printerFrame)
    //*)
END_EVENT_TABLE()

image_printerFrame::image_printerFrame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(image_printerFrame)
    wxMenuItem* MenuItem2;
    wxMenuBar* MenuBar;
    wxMenu* Menu1;
    wxMenuItem* MenuItemQuit;
    wxMenu* Menu2;

    Create(parent, wxID_ANY, _("ImagePrinter"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE|wxVSCROLL|wxHSCROLL|wxALWAYS_SHOW_SB, _T("wxID_ANY"));
    SetClientSize(wxSize(475,461));
    ScrolledWindow1 = new wxScrolledWindow(this, ID_SCROLLEDWINDOW1, wxPoint(272,320), wxDefaultSize, wxVSCROLL|wxHSCROLL, _T("ID_SCROLLEDWINDOW1"));
    StaticBitmap1 = new wxStaticBitmap(ScrolledWindow1, ID_STATICBITMAP1, wxNullBitmap, wxPoint(0,0), wxDefaultSize, wxSIMPLE_BORDER, _T("ID_STATICBITMAP1"));
    MenuBar = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItemOpenFile = new wxMenuItem(Menu1, idMenuOpenFile, _("Open File\tCtrl-O"), _("Open image file"), wxITEM_NORMAL);
    Menu1->Append(MenuItemOpenFile);
    MenuItemCloseFile = new wxMenuItem(Menu1, idMenuCloseFile, _("Close File\tCtrl-W"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(MenuItemCloseFile);
    MenuItemCloseFile->Enable(false);
    Menu1->AppendSeparator();
    MenuItemPrint = new wxMenuItem(Menu1, idMenuPrint, _("Print\tCtrl-P"), _("Print"), wxITEM_NORMAL);
    Menu1->Append(MenuItemPrint);
    MenuItemPrint->Enable(false);
    MenuItemPrintSetup = new wxMenuItem(Menu1, idMenuPrintSetup, _("Print Setup"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(MenuItemPrintSetup);
    MenuItemPrintSetup->Enable(false);
    MenuItemPrintPreview = new wxMenuItem(Menu1, idMenuPrintPreview, _("Print Preview\tShift-Ctrl-P"), _("Print Preview"), wxITEM_NORMAL);
    Menu1->Append(MenuItemPrintPreview);
    MenuItemPrintPreview->Enable(false);
    Menu1->AppendSeparator();
    MenuItemQuit = new wxMenuItem(Menu1, idMenuQuit, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItemQuit);
    MenuBar->Append(Menu1, _("File"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar);
    StatusBar = new wxStatusBar(this, ID_STATUSBAR, wxST_SIZEGRIP|wxSTATIC_BORDER, _T("ID_STATUSBAR"));
    int __wxStatusBarWidths_1[1] = { -2 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar);
    FileDialog = new wxFileDialog(this, _("Select file"), wxEmptyString, wxEmptyString, _("\"Image files (*.bmp;*.gif;*.jpeg;*.jpg;*.png)|*.bmp;*.gif;*.jpeg;*.jpg;*.png"), wxFD_DEFAULT_STYLE|wxFD_FILE_MUST_EXIST, wxDefaultPosition, wxDefaultSize, _T("wxFileDialog"));
    ToolBar1 = new wxToolBar(this, ID_TOOLBAR1, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL|wxNO_BORDER, _T("ID_TOOLBAR1"));
    ToolBarItemOpenFile = ToolBar1->AddTool(ID_TOOLBARITEMOPEN, _("Open File"), wxArtProvider::GetBitmap(wxART_MAKE_ART_ID_FROM_STR(_T("wxART_FILE_OPEN")),wxART_TOOLBAR), wxNullBitmap, wxITEM_NORMAL, _("Open File"), _("Open File"));
    ToolBarItemPrint = ToolBar1->AddTool(ID_TOOLBARITEMPRINT, _("Print Now"), wxArtProvider::GetBitmap(wxART_MAKE_ART_ID_FROM_STR(_T("wxART_PRINT")),wxART_TOOLBAR), wxNullBitmap, wxITEM_NORMAL, _("Print Now"), _("Print Now"));
    ToolBar1->Realize();
    SetToolBar(ToolBar1);
    MenuItemPopupPrint = new wxMenuItem((&MenuPopup), ID_POPUP_PRINT, _("Print"), _("Print"), wxITEM_NORMAL);
    MenuItemPopupPrint->SetBitmap(wxArtProvider::GetBitmap(wxART_MAKE_ART_ID_FROM_STR(_T("wxART_PRINT")),wxART_OTHER));
    MenuPopup.Append(MenuItemPopupPrint);
    Center();

    Connect(idMenuOpenFile,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&image_printerFrame::OnMenuItemOpenFileSelected);
    Connect(idMenuCloseFile,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&image_printerFrame::OnMenuItemCloseFileSelected);
    Connect(idMenuPrint,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&image_printerFrame::OnMenuItemPrint);
    Connect(idMenuPrintSetup,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&image_printerFrame::OnMenuItemPrintSetupSelected);
    Connect(idMenuPrintPreview,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&image_printerFrame::OnMenuItemPrintPreviewSelected);
    Connect(idMenuQuit,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&image_printerFrame::OnQuit);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&image_printerFrame::OnAbout);
    Connect(ID_TOOLBARITEMOPEN,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&image_printerFrame::OnMenuItemOpenFileSelected);
    Connect(ID_TOOLBARITEMPRINT,wxEVT_COMMAND_TOOL_CLICKED,(wxObjectEventFunction)&image_printerFrame::OnToolBarItemPrintClicked);
    Connect(ID_POPUP_PRINT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&image_printerFrame::OnMenuItemPrint);
    Connect(wxID_ANY,wxEVT_CLOSE_WINDOW,(wxObjectEventFunction)&image_printerFrame::OnClose);
    //*)

    // bind Right click event to wxStaticBitmap to image_printerFrame::OnBitmapRightClick
    StaticBitmap1->Bind (wxEVT_RIGHT_UP, &image_printerFrame::OnBitmapRightClick, this);

    // internal data init
    this->print_data = new wxPrintData;
    this->page_setup_data = new wxPageSetupDialogData;

    this->print_data->SetPaperId(wxPAPER_A4);
    this->print_data->SetQuality(600); // set print quality to 600 DPI

    // copy over initial paper size from print record
    (*this->page_setup_data) = *(this->print_data);

    // Set some initial page margins in mm.
    this->page_setup_data->SetMarginTopLeft(wxPoint(0, 0));
    this->page_setup_data->SetMarginBottomRight(wxPoint(0, 0));

    // try to load file_name
    if (!this->file_name.IsEmpty()) {
        this->OpenFile(this->file_name);
    }

    // disable some functions when no image loaded
    if (this->file_name.IsEmpty()) {
        ToolBar1->EnableTool(ID_TOOLBARITEMPRINT, false);
    }
}

image_printerFrame::~image_printerFrame()
{
    //(*Destroy(image_printerFrame)
    //*)
}

void image_printerFrame::OnClose(wxCloseEvent& event)
{
    if (this->print_data) {
        delete this->print_data;
    }

    if (this->page_setup_data) {
        delete this->page_setup_data;
    }

    Destroy(); // you may also do: event.Skip();
               // since the default event handler does call Destroy(), too
}

void image_printerFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void image_printerFrame::OnAbout(wxCommandEvent& event)
{
    wxString description, copyright;

    description.Printf(_("%s prints image fitted in paper size."), APP_NAME);
    copyright.Printf(_("(C) 2014 %s <%s>"), APP_AUTHOR, APP_AUTHOR_MAIL);

    wxAboutDialogInfo info;
    info.SetName(APP_NAME);
    info.SetVersion(APP_VERSION);
    info.SetDescription(description);
    info.SetCopyright(copyright);
    info.SetWebSite(APP_WEB_SITE);
    info.SetLicense(APP_LICENSE);
    wxAboutBox(info);
}

void image_printerFrame::OpenFile(wxString FileName)
{
    wxString strbuf;
    wxFile file;
    wxLongLong file_length;

    if (!this->image.LoadFile(FileName)) {
        wxLogError(_("Can't load image file %s"), FileName);
        this->file_name.Empty();
        return;
    }

    // if image width > height then set printer to wxLANDSCAPE
    if (this->image.GetWidth() > this->image.GetHeight()) {
        this->print_data->SetOrientation(wxLANDSCAPE);
    } else {
        this->print_data->SetOrientation(wxPORTRAIT);
    }

    this->file_name = FileName;
    this->bitmap = wxBitmap(this->image);
    StaticBitmap1->SetBitmap(this->bitmap);
    //bitmap_button = new wxBitmapButton (ScrolledWindow1, ID_BITMAPBUTTON1, this->bitmap, wxPoint(0,0), wxDefaultSize, wxBU_AUTODRAW|wxNO_BORDER, wxDefaultValidator, _T("ID_BITMAPBUTTON1"));

    if (!file.Open (FileName)) {
        return;
    }

    file_length = file.Length();
    file.Close();

    strbuf.Printf (_("%s  Width:%d Height:%d Size:%ld"), FileName, this->image.GetWidth(), this->image.GetHeight(), file_length);
    StatusBar->SetStatusText(strbuf, 0);

    // this part makes the scrollbars show up
    this->ScrolledWindow1->FitInside(); // ask the sizer about the needed size
    this->ScrolledWindow1->Fit();
    this->Fit();

    // init scrolled area size, scrolling speed, etc.
    this->ScrolledWindow1->SetScrollbars(1, 1, image.GetWidth(), image.GetHeight(), 0, 0);
    this->ScrolledWindow1->SetScrollRate(5, 5);

    // enable MenuItem Close File, Print, Print Setup, Print Preview
    this->MenuItemCloseFile->Enable(true);
    this->MenuItemPrint->Enable(true);
    this->MenuItemPrintSetup->Enable(true);
    this->MenuItemPrintPreview->Enable(true);
    this->ToolBar1->EnableTool(ID_TOOLBARITEMPRINT, true);
}

void image_printerFrame::CloseFile(void)
{
    this->file_name.Clear();
    this->image.Destroy();
    this->bitmap.Create(0, 0);
    StaticBitmap1->SetBitmap(this->bitmap);
    StatusBar->SetStatusText("", 0);

    // this part makes the scrollbars show up
    this->ScrolledWindow1->FitInside(); // ask the sizer about the needed size
    this->ScrolledWindow1->Fit();
    this->Fit();

    // init scrolled area size, scrolling speed, etc.
    this->ScrolledWindow1->SetScrollbars(1, 1, 0, 0, 0, 0);

    // disable MenuItem Close File, Print, Print Setup, Print Preview
    this->MenuItemCloseFile->Enable(false);
    this->MenuItemPrint->Enable(false);
    this->MenuItemPrintSetup->Enable(false);
    this->MenuItemPrintPreview->Enable(false);
    this->ToolBar1->EnableTool(ID_TOOLBARITEMPRINT, false);

    this->print_data->SetOrientation(wxPORTRAIT);
}


void image_printerFrame::OnMenuItemOpenFileSelected(wxCommandEvent& event)
{
    if (FileDialog->ShowModal() == wxID_CANCEL) {
        return; // the user changed idea...
    }

    this->OpenFile(FileDialog->GetPath());

    //wxMessageBox(FileDialog->GetPath(), _("You chose..."));
}

void image_printerFrame::OnMenuItemPrint(wxCommandEvent& event)
{
    // return if no image file loaded
    if (this->file_name.IsEmpty()) {
        return;
    }

    wxPrintDialogData printDialogData(*(this->print_data));

    wxPrinter printer(&printDialogData);
    MyPrintout printout(this, wxT("My printout"));

    if (!printer.Print(this, &printout, true /*prompt*/)) {
        if (wxPrinter::GetLastError() == wxPRINTER_ERROR) {
            wxLogError(wxT("There was a problem printing. Perhaps your current printer is not set correctly?"));
        } else {
            //wxLogMessage(wxT("You canceled printing"));
        }
    } else {
        (*(this->print_data)) = printer.GetPrintDialogData().GetPrintData();
    }
}

void image_printerFrame::OnToolBarItemPrintClicked(wxCommandEvent& event)
{
    // return if no image file loaded
    if (this->file_name.IsEmpty()) {
        return;
    }

    wxPrintDialogData printDialogData(*(this->print_data));

    wxPrinter printer(&printDialogData);
    MyPrintout printout(this, wxT("My printout"));

    if (!printer.Print(this, &printout, false /*prompt*/)) {
        if (wxPrinter::GetLastError() == wxPRINTER_ERROR) {
            wxLogError(wxT("There was a problem printing. Perhaps your current printer is not set correctly?"));
        }
    } else {
        (*(this->print_data)) = printer.GetPrintDialogData().GetPrintData();
    }
}

void image_printerFrame::OnMenuItemCloseFileSelected(wxCommandEvent& event)
{
    this->CloseFile();
}

void image_printerFrame::OnMenuItemPrintSetupSelected(wxCommandEvent& event)
{
    *(this->page_setup_data) = *(this->print_data);

    wxPageSetupDialog pageSetupDialog(this, this->page_setup_data);
    pageSetupDialog.ShowModal();

    *(this->print_data) = pageSetupDialog.GetPageSetupDialogData().GetPrintData();
    *(this->page_setup_data) = pageSetupDialog.GetPageSetupDialogData();
}

void image_printerFrame::OnMenuItemPrintPreviewSelected(wxCommandEvent& event)
{
    // return if no image file loaded
    if (this->file_name.IsEmpty()) {
        return;
    }

    // Pass two printout objects: for preview, and possible printing.
    wxPrintDialogData printDialogData(*(this->print_data));
    wxPrintPreview *preview =
        new wxPrintPreview(new MyPrintout(this), new MyPrintout(this), &printDialogData);
    if (!preview->IsOk())
    {
        delete preview;
        wxLogError(wxT("There was a problem previewing.\nPerhaps your current printer is not set correctly?"));
        return;
    }

    wxPreviewFrame *frame =
        //new wxPreviewFrame(preview, this, wxT("Print Preview"), wxPoint(0, 0), wxSize(this->bitmap.GetWidth() + 250, this->bitmap.GetHeight() + 250));
        new wxPreviewFrame(preview, this, wxT("Print Preview"));
    frame->Centre(wxBOTH);
    frame->InitializeWithModality(wxPreviewFrame_AppModal);

    // Find wxPreviewFrame->wxPreviewCanvas (wxScrolledWindow)'s virtual size
    wxWindow *canvas = frame->FindWindowByName(_("canvas"));

    wxSize virtualsize;
    wxSize frame_size = frame->GetBestVirtualSize();

    // if canvas found, set frame size to its virtual size
    if (canvas) {
        virtualsize = canvas->GetVirtualSize();
        //wxLogWarning(_("frame_size  %d %d"), frame_size.GetWidth(), frame_size.GetHeight());
        //wxLogWarning(_("canvas virtualsize %d %d"), virtualsize.GetWidth(), virtualsize.GetHeight());
        if (frame_size.GetWidth() < virtualsize.GetWidth()) {
            frame_size.SetWidth (virtualsize.GetWidth());
        }
        frame_size.SetHeight (frame_size.GetHeight() + virtualsize.GetHeight());
    }

    frame->SetClientSize(frame_size);
    frame->Show();
}


// ----------------------------------------------------------------------------
// MyPrintout
// ----------------------------------------------------------------------------

bool MyPrintout::OnPrintPage(int page)
{
    wxDC *dc = GetDC();

    if ((dc == NULL) || (page != 1)) {
        return false;
    }

    DrawPage();

    // Draw page numbers at top left corner of printable area, sized so that
    // screen size of text matches paper size.
    //MapScreenSizeToPage();

    //dc->DrawText(wxString::Format(wxT("PAGE %d"), page), 0, 0);

    return true;
}

bool MyPrintout::OnBeginDocument(int startPage, int endPage)
{
    if (!wxPrintout::OnBeginDocument(startPage, endPage))
        return false;

    return true;
}

void MyPrintout::GetPageInfo(int *minPage, int *maxPage, int *selPageFrom, int *selPageTo)
{
    *minPage = 1;
    *maxPage = 1;
    *selPageFrom = 1;
    *selPageTo = 1;
}

bool MyPrintout::HasPage(int pageNum)
{
    return (pageNum == 1);
}

void MyPrintout::DrawPage()
{
    // You might use THIS code if you were scaling graphics of known size to fit
    // on the page. The commented-out code illustrates different ways of scaling
    // the graphics.

    // Loaded graphic dimension
    wxCoord maxX = this->image_print_frame->bitmap.GetWidth();
    wxCoord maxY = this->image_print_frame->bitmap.GetHeight();

    // This sets the user scale and origin of the DC so that the image fits
    // within the paper rectangle (but the edges could be cut off by printers
    // that can't print to the edges of the paper -- which is most of them. Use
    // this if your image already has its own margins.
    FitThisSizeToPaper(wxSize(maxX, maxY));
    wxRect fitRect = GetLogicalPaperRect();

    // This sets the user scale and origin of the DC so that the image fits
    // within the page rectangle, which is the printable area on Mac and MSW
    // and is the entire page on other platforms.
//    FitThisSizeToPage(wxSize(maxX, maxY));
//    wxRect fitRect = GetLogicalPageRect();

    // This sets the user scale and origin of the DC so that the image fits
    // within the page margins as specified by g_PageSetupData, which you can
    // change (on some platforms, at least) in the Page Setup dialog. Note that
    // on Mac, the native Page Setup dialog doesn't let you change the margins
    // of a wxPageSetupDialogData object, so you'll have to write your own dialog or
    // use the Mac-only wxMacPageMarginsDialog, as we do in this program.
    //FitThisSizeToPageMargins(wxSize(maxX, maxY), *(image_print_frame->print_data));
    //wxRect fitRect = GetLogicalPageMarginsRect(*(image_print_frame->page_setup_data));

    // This sets the user scale and origin of the DC so that the image appears
    // on the paper at the same size that it appears on screen (i.e., 10-point
    // type on screen is 10-point on the printed page) and is positioned in the
    // top left corner of the page rectangle (just as the screen image appears
    // in the top left corner of the window).
//    MapScreenSizeToPage();
//    wxRect fitRect = GetLogicalPageRect();

    // You could also map the screen image to the entire paper at the same size
    // as it appears on screen.
//    MapScreenSizeToPaper();
//    wxRect fitRect = GetLogicalPaperRect();

    // You might also wish to do you own scaling in order to draw objects at
    // full native device resolution. In this case, you should do the following.
    // Note that you can use the GetLogicalXXXRect() commands to obtain the
    // appropriate rect to scale to.
//    MapScreenSizeToDevice();
//    wxRect fitRect = GetLogicalPageRect();

    // Each of the preceding Fit or Map routines positions the origin so that
    // the drawn image is positioned at the top left corner of the reference
    // rectangle. You can easily center or right- or bottom-justify the image as
    // follows.

    // This offsets the image so that it is centered within the reference
    // rectangle defined above.
    wxCoord xoff = (fitRect.width - maxX) / 2;
    wxCoord yoff = (fitRect.height - maxY) / 2;
    OffsetLogicalOrigin(xoff, yoff);

    // This offsets the image so that it is positioned at the bottom right of
    // the reference rectangle defined above.
//    wxCoord xoff = (fitRect.width - maxX);
//    wxCoord yoff = (fitRect.height - maxY);
//    OffsetLogicalOrigin(xoff, yoff);

    // draw bitmap to DC
    GetDC()->DrawBitmap(this->image_print_frame->bitmap, 0, 0);
}

void image_printerFrame::OnBitmapRightClick(wxMouseEvent& event)
{
    PopupMenu (&MenuPopup);
}
