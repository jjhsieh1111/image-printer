/***************************************************************
 * Name:      image_printerMain.h
 * Purpose:   Defines Application Frame
 * Author:    JJ Hsieh ()
 * Created:   2014-08-01
 * Copyright: JJ Hsieh ()
 * License:
 **************************************************************/

#ifndef IMAGE_PRINTERMAIN_H
#define IMAGE_PRINTERMAIN_H

#include <wx/file.h>
#include <wx/print.h>
#include <wx/printdlg.h>

//(*Headers(image_printerFrame)
#include <wx/toolbar.h>
#include <wx/menu.h>
#include <wx/filedlg.h>
#include <wx/scrolwin.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/statbmp.h>
//*)

class image_printerFrame: public wxFrame
{
    public:

        image_printerFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~image_printerFrame();

        void OpenFile (wxString FileName);
        void CloseFile (void);

        wxString file_name;
        wxBitmap bitmap;
        wxImage image;
        wxBitmapButton *bitmap_button = NULL;

        // print data, to remember settings during the session
        wxPrintData *print_data = NULL;

        // page setup data
        wxPageSetupDialogData* page_setup_data = NULL;

    private:

        //(*Handlers(image_printerFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnMenuItemOpenFileSelected(wxCommandEvent& event);
        void OnMenuItemPrint(wxCommandEvent& event);
        void OnScrolledWindow1Resize(wxSizeEvent& event);
        void OnMenuItemCloseFileSelected(wxCommandEvent& event);
        void OnMenuItemPrintSetupSelected(wxCommandEvent& event);
        void OnClose(wxCloseEvent& event);
        void OnMenuItemPrintPreviewSelected(wxCommandEvent& event);
        void OnToolBarItemPrintClicked(wxCommandEvent& event);
        void OnScrolledWindow1RightUp(wxMouseEvent& event);
        void OnRightUp(wxMouseEvent& event);
        //*)

        void OnBitmapRightClick(wxMouseEvent& event);

        //(*Identifiers(image_printerFrame)
        static const long ID_STATICBITMAP1;
        static const long ID_SCROLLEDWINDOW1;
        static const long idMenuOpenFile;
        static const long idMenuCloseFile;
        static const long idMenuPrint;
        static const long idMenuPrintSetup;
        static const long idMenuPrintPreview;
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR;
        static const long ID_TOOLBARITEMOPEN;
        static const long ID_TOOLBARITEMPRINT;
        static const long ID_TOOLBAR1;
        static const long ID_POPUP_PRINT;
        //*)

        static const long ID_BITMAPBUTTON1;

        //(*Declarations(image_printerFrame)
        wxFileDialog* FileDialog;
        wxMenuItem* MenuItemPrint;
        wxMenuItem* MenuItemPrintPreview;
        wxScrolledWindow* ScrolledWindow1;
        wxMenuItem* MenuItemCloseFile;
        wxToolBarToolBase* ToolBarItemOpenFile;
        wxToolBar* ToolBar1;
        wxMenuItem* MenuItemPrintSetup;
        wxMenu MenuPopup;
        wxToolBarToolBase* ToolBarItemPrint;
        wxMenuItem* MenuItemPopupPrint;
        wxStaticBitmap* StaticBitmap1;
        wxStatusBar* StatusBar;
        wxMenuItem* MenuItemOpenFile;
        //*)

        DECLARE_EVENT_TABLE()
};

// Defines a new printout class to print our document
class MyPrintout: public wxPrintout
{
    public:
        MyPrintout(image_printerFrame* frame, const wxString &title = wxT("My printout"))
            : wxPrintout(title) { image_print_frame = frame; }

        virtual bool OnPrintPage(int page);
        virtual bool HasPage(int page);
        virtual bool OnBeginDocument(int startPage, int endPage);
        virtual void GetPageInfo(int *minPage, int *maxPage, int *selPageFrom, int *selPageTo);

        void DrawPage();

    private:
        image_printerFrame *image_print_frame;
};

#endif // IMAGE_PRINTERMAIN_H
